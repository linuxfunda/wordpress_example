default['apache']['listen']            = ['*:80']
default['apache']['default_site_port']    = '80'

default['apache']['proxy']['require']    = 'all denied'
default['apache']['proxy']['order']      = 'deny,allow'
default['apache']['proxy']['deny_from']  = 'none'
default['apache']['proxy']['allow_from'] = 'all'

default['LinuxFunda']['mysql']['root_password'] = "Ch4ng3me"
default['LinuxFunda']['mysql']['version'] = "5.5"
default['LinuxFunda']['mysql']['data_dir'] = "/data"
default['LinuxFunda']['mysql']['bind_address'] = "0.0.0.0"
default['LinuxFunda']['mysql']['service_name'] = "linuxfunda"
default['LinuxFunda']['mysql']['port'] = "3306"

default['LinuxFunda']['wordpress']['version'] = 'latest'
default['LinuxFunda']['wordpress']['server_name'] = "localhost"
default['LinuxFunda']['wordpress']['parent_dir'] = '/var/www'
default['LinuxFunda']['wordpress']['dir'] = "#{node['LinuxFunda']['wordpress']['parent_dir']}/wordpress"
default['LinuxFunda']['wordpress']['url'] = "https://wordpress.org/wordpress-#{node['LinuxFunda']['wordpress']['version']}.tar.gz"
default['LinuxFunda']['wordpress']['db']['name'] = "wordpressdb"
default['LinuxFunda']['wordpress']['db']['user'] = "wordpressuser"
default['LinuxFunda']['wordpress']['db']['pass'] = nil
default['LinuxFunda']['wordpress']['db']['prefix'] = 'wp_'
default['LinuxFunda']['wordpress']['install']['user'] = node['apache']['user']
default['LinuxFunda']['wordpress']['install']['group'] = node['apache']['group']
default['LinuxFunda']['wordpress']['allow_multisite'] = false
default['LinuxFunda']['wordpress']['languages']['lang'] = ''
default['LinuxFunda']['wordpress']['db']['host'] = 'localhost'
default['LinuxFunda']['wordpress']['db']['port'] = '3306'
default['LinuxFunda']['wordpress']['wp_config_options'] = {}

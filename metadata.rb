name             'LinuxFunda'
maintainer       'Tapas Mishra'
maintainer_email 'tapas.mishra@linuxfunda.com'
license          'Apache 2.0'
description      'Installs/Configures LinuxFunda'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends "apache2"
depends "database"
depends "mysql"
depends "mysql2_chef_gem"
depends "php"
depends "tar"

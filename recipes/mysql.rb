#
# Cookbook Name:: LinuxFunda
# Recipe:: mysql
#
# Copyright 2014, Tapas Mishra
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

user 'tp' do
  action :create 
end

directory "#{node['LinuxFunda']['mysql']['data_dir']}" do 
  owner 'tp'
  action :create
end

mysql_service "#{node['LinuxFunda']['mysql']['service_name']}" do
  version node['LinuxFunda']['mysql']['version']
  bind_address node['LinuxFunda']['mysql']['bind_address']
  port node['LinuxFunda']['mysql']['port']
  data_dir node['LinuxFunda']['mysql']['data_dir']
  initial_root_password node['LinuxFunda']['mysql']['root_password']
  run_user 'tp'
  action [:create, :start]
end

socket = "/var/run/mysql-#{node['LinuxFunda']['mysql']['service_name']}/mysqld.sock"

if node['platform_family'] == 'debian'
  link '/var/run/mysqld/mysqld.sock' do
    to socket
    not_if 'test -f /var/run/mysqld/mysqld.sock'
  end
elsif node['platform_family'] == 'rhel'
  link '/var/lib/mysql/mysql.sock' do
    to socket
    not_if 'test -f /var/lib/mysql/mysql.sock'
  end
end

mysql_connection_info = {
  :host     => 'localhost',
  :username => 'root',
  :socket   => socket,
  :password => node['LinuxFunda']['mysql']['root_password']
}

mysql2_chef_gem 'default' do
  action :install
end

mysql_database node['LinuxFunda']['wordpress']['db']['name'] do
  connection  mysql_connection_info
  action      :create
end

mysql_database_user node['LinuxFunda']['wordpress']['db']['user'] do
  connection    mysql_connection_info
  password      node['LinuxFunda']['wordpress']['db']['pass']
  host          'localhost'
  database_name node['LinuxFunda']['wordpress']['db']['name']
  action        :create
end

mysql_database_user node['LinuxFunda']['wordpress']['db']['user'] do
  connection    mysql_connection_info
  database_name node['LinuxFunda']['wordpress']['db']['name']
  privileges    [:all]
  action        :grant
end

mysql_client "#{node['LinuxFunda']['mysql']['service_name']}" do
  action :create
end

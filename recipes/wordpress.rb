#
# Cookbook Name:: LinuxFunda
# Recipe:: apache2
#
# Copyright 2014, Tapas Mishra
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

::Chef::Recipe.send(:include, Opscode::OpenSSL::Password)
node.set_unless['LinuxFunda']['wordpress']['keys']['auth'] = secure_password
node.set_unless['LinuxFunda']['wordpress']['keys']['secure_auth'] = secure_password
node.set_unless['LinuxFunda']['wordpress']['keys']['logged_in'] = secure_password
node.set_unless['LinuxFunda']['wordpress']['keys']['nonce'] = secure_password
node.set_unless['LinuxFunda']['wordpress']['salt']['auth'] = secure_password
node.set_unless['LinuxFunda']['wordpress']['salt']['secure_auth'] = secure_password
node.set_unless['LinuxFunda']['wordpress']['salt']['logged_in'] = secure_password
node.set_unless['LinuxFunda']['wordpress']['salt']['nonce'] = secure_password
node.save unless Chef::Config[:solo]


directory node['LinuxFunda']['wordpress']['dir'] do
  action :create
  recursive true
  owner node['LinuxFunda']['wordpress']['install']['user']
  group node['LinuxFunda']['wordpress']['install']['group']
  mode  '00755'
end

tar_extract node['LinuxFunda']['wordpress']['url'] do
  target_dir node['LinuxFunda']['wordpress']['dir']
  creates File.join(node['LinuxFunda']['wordpress']['dir'], 'index.php')
  user node['LinuxFunda']['wordpress']['install']['user']
  group node['LinuxFunda']['wordpress']['install']['group']
  tar_flags [ '--strip-components 1' ]
end

template "#{node['LinuxFunda']['wordpress']['dir']}/wp-config.php" do
  source 'wp-config.php.erb'
  mode 0644
  variables(
    :db_name          => node['LinuxFunda']['wordpress']['db']['name'],
    :db_user          => node['LinuxFunda']['wordpress']['db']['user'],
    :db_password      => node['LinuxFunda']['wordpress']['db']['pass'],
    :db_host          => node['LinuxFunda']['wordpress']['db']['host'],
    :db_prefix        => node['LinuxFunda']['wordpress']['db']['prefix'],
    :auth_key         => node['LinuxFunda']['wordpress']['keys']['auth'],
    :secure_auth_key  => node['LinuxFunda']['wordpress']['keys']['secure_auth'],
    :logged_in_key    => node['LinuxFunda']['wordpress']['keys']['logged_in'],
    :nonce_key        => node['LinuxFunda']['wordpress']['keys']['nonce'],
    :auth_salt        => node['LinuxFunda']['wordpress']['salt']['auth'],
    :secure_auth_salt => node['LinuxFunda']['wordpress']['salt']['secure_auth'],
    :logged_in_salt   => node['LinuxFunda']['wordpress']['salt']['logged_in'],
    :nonce_salt       => node['LinuxFunda']['wordpress']['salt']['nonce'],
    :lang             => node['LinuxFunda']['wordpress']['languages']['lang'],
    :allow_multisite  => node['LinuxFunda']['wordpress']['allow_multisite'],
    :wp_config_options => node['LinuxFunda']['wordpress']['wp_config_options']
  )
  owner node['LinuxFunda']['wordpress']['install']['user']
  group node['LinuxFunda']['wordpress']['install']['group']
  action :create
end
